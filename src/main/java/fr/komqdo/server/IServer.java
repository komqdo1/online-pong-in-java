package fr.komqdo.server;

import java.util.ArrayList;
import java.util.List;

import fr.komqdo.utils.SocketServer.Connected;

/**
 * Contient les comportements du serveur sans le code java.
 * 
 * @author Komqdo
 */

public interface IServer {

	/**
	 * @return La liste des utilisateurs connectés au serveur.
	 * @author Komqdo
	 */

	public List<Connected> getConnecteds();

	/**
	 * @param user_id L'ID de l'utilisateur.
	 * 
	 * @return True si l'utilisateur spécifié est en ligne.
	 * 
	 * @author Komqdo
	 */

	public default boolean isOnline(int user_id) {
		return (getConnected(user_id) != null);
	}

	/**
	 * @param user_id l'ID de l'utilisateur.
	 * 
	 * @return L'utilisateur attaché à l'ID specifié en paramètre.
	 * 
	 * @author Komqdo
	 */

	public default Connected getConnected(int user_id) {
		for (Connected tmp : new ArrayList<>(getConnecteds())) {
			if (tmp.user_id == user_id) {
				return (tmp);
			}
		}
		return (null);
	}

	/**
	 * Envoie un packet PacketOutTimeUpdate à l'utilisateur spécifié
	 * en paramètre.
	 * 
	 * Le packet PacketOutTimeUpdate indique au client s'il il est en jeu ou non.
	 * Il indique également au client les coordonnées de la balle et de son adversaire.
	 * Il contient également le temps de jeu du match.
	 * 
	 * C'est ce packet qui permet au client de mettre à jour son status "en jeu" et de voir
	 * la balle et la raquette adverse bouger.
	 * 
	 * @param connected L'utilisateur à qui envoyer le packet PacketOutTimeUpdate.
	 * 
	 * @author Komqdo
	 */

	public void sendTimePacket(Connected connected);

	/**
	 * Essaie de démarrer le match pour l'utilisateur spécifié en paramètre.
	 * 
	 * Si l'utilisateur a fait une demande, le serveur essaie de lui trouver son adversaire selon
	 * si la demande.
	 * 
	 * Si l'adversaire est trouvé, un match est démarré entre l'utilisateur spécifié et son adversaire.
	 * 
	 * @param connected L'utilisateur à qui on tente de démarrer son match.
	 * 
	 * @author Komqdo
	 */

	public void checkMatchStart(Connected connected);

	/**
	 * Mets à jour les matches actuellement en cours (Position de la balle,
	 * le temps de jeu, ...).
	 * 
	 * @author Komqdo
	 */

	public void updateMatches();

	/**
	 * Mets à jour le serveur (Matches, déconnexion des utilisateurs...)
	 * @author Komqdo
	 */

	public default void update() {

		// Mets à jour les utilisateurs.
		
		for (Connected connected : new ArrayList<>(getConnecteds())) {

			// Envoie le PacketOutTimeUpdate.

			sendTimePacket(connected);
			
			// Déconnecte l'utilisateur s'il n'a pas répondu depuis 5 secondes.
			
			if (connected.isTimeout()) {
				connected.close();
				System.out.println("[LOG] " + connected.socket + ": timed out.");
				continue ;
			}

			// Essaie de trouver et lancer le match pour l'utilisateur.

			checkMatchStart(connected);
		}

		// Mets à jour les matches en cours.

		updateMatches();
	}
}
