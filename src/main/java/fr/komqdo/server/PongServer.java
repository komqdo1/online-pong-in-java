package fr.komqdo.server;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fr.komqdo.area.Area;
import fr.komqdo.area.Area.Racket;
import fr.komqdo.packet.Packet;
import fr.komqdo.packet.in.PacketInDual;
import fr.komqdo.packet.in.PacketInDualCancel;
import fr.komqdo.packet.in.PacketInHandshake;
import fr.komqdo.packet.in.PacketInKeepAlive;
import fr.komqdo.packet.out.PacketOutTimeUpdate;
import fr.komqdo.utils.SocketServer;
import fr.komqdo.utils.TickValue;

/**
 * Implémentation Java du serveur.
 * 
 * Hérite de SocketServer pour les fonctionnalités réseau (sendPacket, receivePacket).
 * 
 * @author Komqdo
 */

public class PongServer extends SocketServer implements IServer {

	private Set<Match> matches;		// Contient la liste des matches en cours.

	public PongServer() {

		this.matches = new HashSet<>();

	}

	/**
	 * Implémentation Java de la fonction receivePacket, appelée quand un packet
	 * est reçu de la part d'un client.
	 * 
	 * @param connected L'envoyeur du packet reçu.
	 * @param packet Le packet reçu.
	 * 
	 * @author Komqdo
	 */
	
	@Override
	public void receivePacket(Connected connected, Packet packet) {
		
		/**
		 * Réception de PacketInHandshake, qui permet à un utilisateur de s'identifier
		 * auprès du serveur.
		 */

		if (packet instanceof PacketInHandshake) {
			final PacketInHandshake tmp = (PacketInHandshake) packet;

			System.out.println("[LOG] " + connected.socket + ": handshaked: user id: " + tmp.user_id);

			connected.user_id = tmp.user_id;
			return ;
		}

		/**
		 * Réception de PacketInKeepAlive, permet au serveur de s'assurer que le client
		 * est encore connecté au serveur, et permet au client de donner au serveur la
		 * coordonnée de sa raquette.
		 */
		
		if (packet instanceof PacketInKeepAlive) {

			final PacketInKeepAlive tmp = (PacketInKeepAlive) packet;

			connected.last_socket_timestamp = System.currentTimeMillis();
			connected.ping = (int) (connected.last_socket_timestamp - tmp.timestamp);

			if (connected.hasMatch() && tmp.y_pcent != null) {
				connected.match.getRacket(connected).getLocation().setY(tmp.y_pcent);
			}

			return ;
		}

		/**
		 * Réception du packet PacketInDual, qui permet à un utilisateur de demander
		 * au serveur d'affronter un utilisateur spécifique, ou random (en précisant l'ID 0).
		 * 
		 * Si l'utilisateur est déjà en combat, rien ne se passe.
		 */

		if (packet instanceof PacketInDual) {
			
			final PacketInDual tmp = (PacketInDual) packet;
			
			if (connected.hasMatch()) {
				return ;
			}

			if (connected.user_id == tmp.opponent_id) {
				System.err.println("[ERR] user dual himself.");
				System.exit(1);
				return ;
			}

			connected.opponent_id = tmp.opponent_id;
			return ;
		}

		/**
		 * Réception du packet PacketInDualCancel, qui permet à un utilisateur de
		 * demander l'annulation de sa demande de combat (Match making ou Friend dual).
		 * 
		 * Si le combat a démarré, il s'arrête.
		 */

		if (packet instanceof PacketInDualCancel) {

			if (connected.opponent_id == null) {
				return ;
			}

			if (connected.hasMatch()) {
				connected.match.close();
				return ;
			}

			connected.opponent_id = null;
		}
	}

	/**
	 * Implémentation Java de la fonction sendTimePacket(connected), qui envoie un packet
	 * PacketOutTimeUpdate à l'utilisateur spécifié.
	 * 
	 * Si l'utilisateur est en combat, le servuer lui envoie les coordonnées de la balle, la coordonnée
	 * de la raquette de son adversaire, le temps du match, et un bool indiquant s'il est en jeu ou non.
	 * 
	 * Si l'utilisateur n'est pas en combat, il reçoit simplement un bool indiquant qu'il n'est pas en jeu.
	 * 
	 * @param connected L'utilisateur à qui ont souhaite envoyer le packet PacketOutTimeUpdate.
	 * 
	 * @author Komqdo
	 */

	@Override
	public void sendTimePacket(Connected connected) {

		Integer ball_x_pcent = null;
		Integer ball_y_pcent = null;
		Boolean to_left = null;
		Integer opponent_y_pcent = null;
		Integer match_time = null;

		if (connected.hasMatch()) {

			ball_x_pcent = connected.match.getArea().getBall().getLocation().getXPercent();
			ball_y_pcent = connected.match.getArea().getBall().getLocation().getYPercent();
			to_left = connected.match.user1.user_id != connected.getUserId();
			
			final Racket opponent = to_left ? connected.match.getArea().getPlayer() : connected.match.getArea().getOpponent();
			
			opponent_y_pcent = opponent.getLocation().getYPercent();

			match_time = connected.match.time.getTickValue();
		}

		final PacketOutTimeUpdate packet = new PacketOutTimeUpdate(
				ball_x_pcent, ball_y_pcent, to_left, opponent_y_pcent, connected.opponent_id,
				connected.hasMatch(), match_time);
				
		sendPacket(connected, packet);
	}

	/**
	 * Implémentation Java de la fonction checkMatchStart(connected), qui permet
	 * de checker dans le cas ou l'utilisateur spécifié a fait une demande de match,
	 * si un adversaire (ou l'adversaire) est disponible.
	 * 
	 * Si un adversaire est trouvé, le match se démarre entre les deux joueurs.
	 * 
	 * @author Komqdo
	 */

	@Override
	public void checkMatchStart(Connected connected) {

		if (!connected.hasMatch() && connected.opponent_id != null) {

			if (connected.opponent_id == 0) {
				
				final Connected opponent = getWaitingOpponent(connected.user_id);
				
				System.out.println("[LOG] " + connected.socket + ": searching opponent...: " + opponent);

				if (opponent != null) {
					System.out.println("[LOG] " + connected.socket + ": found opponent: " + opponent);
					createMatch(connected, opponent);
					return ;
				}

			} else {

				final Connected opponent = getConnected(connected.opponent_id);
				
				if (opponent != null && opponent.opponent_id != null && opponent.opponent_id == connected.user_id) {
					
					createMatch(connected, opponent);
					return ;
				}
			}
		}
	}

	/**
	 * Implémentation Java de la fonction updateMatches, qui permet de mettre
	 * à jour les matches en cours (Coordonnées de la balle, temps de jeu, ...).
	 * 
	 * @author Komqdo
	 */

	@Override
	public void updateMatches() {
		for (Match match : new ArrayList<>(matches)) {
			match.update();
		}
	}

	/**
	 * Implémentation Java de la fonction getConnecteds, qui renvoie
	 * la liste des utilisateurs connectés au serveur, néccessaire dans
	 * la logique du serveur (IServer).
	 * 
	 * @author Komqdo
	 */

	@Override
	public List<Connected> getConnecteds() {
		return (connecteds);
	}

	/**
	 * Créer un match entre l'utilisateur et l'adversaire, spécifiés en paramètre.
	 * 
	 * @param connected Le premier joueur du match.
	 * @param opponent Le deuxième joueur du match.
	 * 
	 * @author Komqdo
	 */

	private void createMatch(Connected connected, Connected opponent) {
		
		if (connected.hasMatch() || opponent.hasMatch())
			return ;

		final Match match = new Match(connected, opponent);
		
		match.init();
	}

	/**
	 * @param searcher_id L'ID de l'utilisateur pour qui on cherche un adversaire.
	 * 
	 * @return Un adversaire si trouvé, pour l'utilisateur spécifié en paramètre, ou null si
	 * aucun adversaire n'a été trouvé.
	 * 
	 * @author Komqdo
	 */

	private Connected getWaitingOpponent(int searcher_id) {

		for (Connected tmp : new ArrayList<>(connecteds)) {
			
			if (tmp.user_id == searcher_id) {
				continue ;
			}

			if (tmp.opponent_id != null && tmp.opponent_id == 0 && !tmp.hasMatch()) {
				return (tmp);
			}
		}
		return (null);
	}

	/**
	 * Représente un match.
	 * 
	 * @author Komqdo
	 */

	public class Match {
		
		public Connected user1;			// Le premier joueur (À gauche).
		public Connected user2;			// Le deuxième joueur (À droite).
		public boolean closed;			// Si le match est fermé.

		public TickValue time;			// Le temps de jeu.

		private boolean ball_to_left;	// Si la balle va vers la gauche ou non.
		private double ball_speed = 2;	// La vitesse de la balle.
		public double ball_angle = 0;	// L'angle de la balle.

		private Area area;				// L'arène contenant les entités (Raquettes, balle...).

		public Match(Connected user1, Connected user2) {
			
			this.time = new TickValue(0);
			this.user1 = user1;
			this.user1.match = this;
			this.user2 = user2;
			this.user2.match = this;
			this.area = new Area(user1.getUserId(), user2.getUserId());

			// Ajoute ce match à la liste des matches du serveur.
			PongServer.this.matches.add(this);
		}

		/**
		 * Ferme le match.
		 * @author Komqdo
		 */

		public void close() {

			if (this.closed)
				return ;

			System.out.println("[LOG] " + user1.socket + ": match closed.");
			System.out.println("[LOG] " + user2.socket + ": match closed.");

			// Supprime ce match de la liste des matches du serveur.
			
			PongServer.this.matches.remove(this);
			
			this.closed = true;
			
			this.user1.opponent_id = null;
			this.user1.match = null;

			this.user2.opponent_id = null;
			this.user2.match = null;
		}

		/**
		 * Initialise le match.
		 * @author Komqdo
		 */

		public void init() {
			System.out.println("[LOG] " + user1.socket + ": match initialization.");
			System.out.println("[LOG] " + user2.socket + ": match initialization.");
			user1.opponent_id = user2.user_id;
			user2.opponent_id = user1.user_id;
		}

		/**
		 * Mets à jour l'angle de la balle, selon la raquette spécifiée en paramètre.
		 * 
		 * @param racket La raquette en collision avec la balle.
		 * 
		 * @author Komqdo
		 */

		public void updateBallAngle(Racket racket) {
			double positionRelative = getArea().getBall().getLocation().getY() - racket.getLocation().getY();
			double positionNormalisee = positionRelative / (getArea().racketSize().getHeight() / 2);
			double angleMax = 75;
			double angle = positionNormalisee * angleMax;
			double angleRad = Math.toRadians(angle);
			this.ball_angle = Math.min(Math.max(Math.toRadians(-angleMax), angleRad), Math.toRadians(25));
		}

		/**
		 * Mets à jour le match (Coordonnées de la balle, le temps, les collisions, ...).
		 * 
		 * @author Komqdo
		 */
		
		 public void update() {
			
			// Mise à jour de la position de la balle.

			final int direction = !this.ball_to_left ? 1 : -1;
			
			getArea().getBall().getLocation().addX((int) (direction * ball_speed * Math.cos(ball_angle)));
			getArea().getBall().getLocation().addY((int) (ball_speed * Math.sin(ball_angle)));
			
			// Faire ricocher la balle si elle est contre le mur du haut ou du bas.

			if (getArea().getBall().getLocation().getYPercent() == 0) {
				this.ball_angle = -this.ball_angle;
			}

			if (getArea().getBall().getLocation().getYPercent() == 100) {
				this.ball_angle = -this.ball_angle;
			}

			// Collision entre la raquette des joueurs et la balle.

			boolean collision = false;

			if (getArea().getBall().getLocation().getXPercent() == 100 && !this.ball_to_left) {
				if (getArea().getBall().getLocation().isYRange(getArea().getOpponent().getLocation(), getArea().ballSize(), getArea().racketSize())) {
					System.out.println("[LOG] detected collision (user 2)");
					updateBallAngle(getArea().getOpponent());
					collision = true;
				}
			}

			if (getArea().getBall().getLocation().getXPercent() == 0 && this.ball_to_left) {
				if (getArea().getBall().getLocation().isYRange(getArea().getPlayer().getLocation(), getArea().ballSize(), getArea().racketSize())) {
					System.out.println("[LOG] detected collision (user 1)");
					updateBallAngle(getArea().getPlayer());
					collision = true;
				}
			}

			if (collision) {
				this.ball_to_left = !this.ball_to_left;
				
				if (ball_speed <= 4) {
					ball_speed *= 1.12;
				} else if (ball_speed <= 8) {
					ball_speed *= 1.05;
				} else if (ball_speed <= 15) {
					ball_speed *= 1.01;
				}

				if (ball_speed > 15) {
					ball_speed = 15;
				}
			}

			// Mise à jour du temps de la partie.

			this.time.incrementValue();
		}

		/**
		 * @param connected L'utilisateur dont on souhaite récupérer la raquette.
		 * 
		 * @return La raquette associée à l'utilisateur spécifié en paramètre.
		 * 
		 * @author Komqdo
		 */

		public Racket getRacket(Connected connected) {
			if (getArea().getPlayer().getUserId() == connected.getUserId())
				return (getArea().getPlayer());

			if (getArea().getOpponent().getUserId() == connected.getUserId())
				return (getArea().getOpponent());

			return (null);
		}

		public Area getArea() { return (area); }
	}

	/**
	 * Classe "Main" pour lancer le serveur.
	 * @author Komqdo
	 */

	public static class Main {
		
		public static void main(String[] args) {
			
			// Créer un nouveau serveur.

			final PongServer server = new PongServer();

			// Mettre à jour le serveur tous les 50 ms.

			while (true) {
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				server.update();
			}
		}
	}
}
