package fr.komqdo.client;

import fr.komqdo.area.Area;
import fr.komqdo.packet.Packet;
import fr.komqdo.packet.in.PacketInDual;
import fr.komqdo.packet.in.PacketInDualCancel;
import fr.komqdo.packet.in.PacketInHandshake;
import fr.komqdo.packet.in.PacketInKeepAlive;
import fr.komqdo.packet.out.PacketOutTimeUpdate;
import fr.komqdo.utils.SocketClient;

/**
 * Implémentation Java du client.
 * 
 * Hérite de SocketClient pour les fonctionnalités réseau (sendPacket, receivePacket).
 * 
 * @author Komqdo
 */

public class PongClient extends SocketClient implements IClient {

	private int user_id;				// ID de l'utilisateur du client.
	private GameWindow game_window;		// Fenêtre du jeu valeurée quand un match est en cours.
	private MenuWindow menu_window;		// Fenêtre du menu de jeu avec les boutons "Match making" et "Dual friend".

	public PongClient(int user_id) {

		this.user_id = user_id;
		this.menu_window = new MenuWindow(this, user_id);
	}

	/**
	 * Implémentation Java de la fonction receivePacket, appelée quand un packet
	 * est reçu de la part du serveur.
	 * 
	 * @author Komqdo
	 */

	@Override
	public void receivePacket(Packet packet) {
		
		/**
		 * Réception de PacketOutTimeUpdate, qui informe le client sur le status "en jeu" et
		 * sur la position de la balle, la raquette de l'adversaire, et le temps de jeu du match.
		 * 
		 * Invoque setPlaying et setIdle basé sur le status "en jeu" que le
		 * serveur donne au client.
		 * 
		 * Si le joueur est en jeu, les informations sur la balle, la direction de la balle (to_left),
		 * et la hauteur de la raquette de l'adversaire seront valeurées.
		 * 
		 * Le temps de jeu du match peut servir pour afficher un chrono de début de partie
		 * par exemple (En ajustant aussi le serveur).
		 */

		if (packet instanceof PacketOutTimeUpdate) {

			final PacketOutTimeUpdate tmp = (PacketOutTimeUpdate) packet;

			if (tmp.isPlaying()) {

				if (getGameWindow() == null) {
					setPlaying(tmp.opponent_id);
				}

				if (tmp.ball_x_pcent != null)
					getGameWindow().getArea().getBall().getLocation().setX(tmp.ball_x_pcent);
				
				if (tmp.ball_y_pcent != null)
					getGameWindow().getArea().getBall().getLocation().setY(tmp.ball_y_pcent);

				if (tmp.to_left != null)
					getGameWindow().getArea().getBall().getLocation().setToLeft(tmp.to_left);
				
				if (tmp.opponent_y_pcent != null)
					getGameWindow().getArea().getOpponent().getLocation().setY(tmp.opponent_y_pcent);
				
				System.out.println("[LOG] status: playing since " + tmp.time + " seconds agains user " + tmp.opponent_id + " .");
				
			} else {

				setIdle();

				System.out.println("[LOG] status: idle.");
			}
		}
	}

	/**
	 * Implementation Java de la fonction repaint, appelée dans la fonction update de IClient.
	 * Cette fonction mets à jour l'affichage du client.
	 * 
	 * @author Komqdo
	 */

	@Override
	public void repaint() {
		
		// Si le client est en jeu, il a une fenêtre de jeu à mettre à jour.
		if (getGameWindow() != null)
			getGameWindow().repaint();
	}

	/**
	 * Implémentation Java de la fonction sendKeepAlivePacket, appelée dans la fonction update de IClient.
	 * 
	 * Cette fonction envoi au serveur un packet KeepAlive qui informe le serveur que le client est
	 * toujours connecté, et la position de la raquette du client (si il est en jeu).
	 * 
	 * @author Komqdo
	 */

	@Override
	public void sendKeepAlivePacket() {

		Integer racket_y_pcent = null;
		
		// Si le client est en jeu, on donne une valeur à racket_y_pcent.

		if (getGameWindow() != null)
			racket_y_pcent = getGameWindow().getArea().getPlayer().getLocation().getYPercent();
		
		sendPacket(new PacketInKeepAlive(racket_y_pcent));
	}

	/**
	 * Implémentation Java de la fonction sendDualPacket, appelée quand l'utilisateur clique
	 * sur le bouton "Match making" dans MenuWindow.
	 * 
	 * Le packet PacketInDual(0) informe le serveur que le client veut affronter un adversaire.
	 * On précise l'ID 0 pour que le serveur choisisse un adversaire au hasard.
	 * 
	 * L'ID 0 n'existe jamais en base de données.
	 * 
	 * @author Komqdo
	 */

	@Override
	public void sendDualPacket() {
		sendPacket(new PacketInDual(0));
	}

	/**
	 * Implémentation Java de la fonction sendDualPacket(opponent_id), appelée quand l'utilisateur clique
	 * sur le bouton "Dual friend" dans MenuWindow.
	 * 
	 * Le packet PacketInDual(opponent_id) informe le serveur que le client veut affronter l'adversaire spécifié par
	 * l'ID specifié par opponent_id.
	 * 
	 * @author Komqdo
	 */

	@Override
	public void sendDualPacket(int opponent_id) {
		sendPacket(new PacketInDual(opponent_id));
	}

	/**
	 * Implémentation Java de la fonction sendDualCancelPacket (Pas utilisée ici, pourrait être utilisé quand le client
	 * clique sur un bouton "cancel" par exemple).
	 * 
	 * Envoie une requête pour indiquer au serveur que le client veut annuler sa demande de combat (Match making ou Dual friend).
	 * 
	 * @author Komqdo
	 */

	@Override
	public void sendDualCancelPacket() {
		sendPacket(new PacketInDualCancel());
	}

	/**
	 * Implémentation Java de setIdle, appelée au moment de la reception d'un PacketOutTimeUpdate
	 * de la part du serveur, en cas de changement de status "en jeu".
	 * 
	 * Si la fenêtre de jeu est ouverte, on la ferme puis on efface game_window.
	 * 
	 * @author Komqdo
	 */

	@Override
	public void setIdle() {
		if (getGameWindow() != null) {
			getGameWindow().close();
			this.game_window = null;
		}
	}

	/**
	 * Implémentation Java de setPlaying, appelée au moment de la reception d'un PacketOutTimeUpdate
	 * de la part du serveur, en cas de changement de status "en jeu".
	 * 
	 * Créer une nouvelle fenetre, avec une nouvelle Area entre le client et son adversaire.
	 * La fenetre va s'afficher toute seule lors de sa création, son affichage sera mis à jour via la fonction
	 * update de IClient, et la position de la balle et de la raquette de l'adversaire est mit à jour lors
	 * de la reception du packet PacketOutTimeUpdate du client par le serveur.
	 * 
	 * @param opponent_id L'ID de l'adversaire, donné par le serveur.
	 * 
	 * @author Komqdo
	 */

	@Override
	public void setPlaying(int opponent_id) {
		this.game_window = new GameWindow(new Area(this.user_id, opponent_id), this.user_id);
	}

	// Getters

	public GameWindow getGameWindow() { return (game_window); }
	public MenuWindow getMenuWindow() { return (menu_window); }

	/**
	 * Classe "Main" pour lancer le client.
	 * @author Komqdo
	 */

	public static class Main {
		
		public static void main(String[] args) {

			// Créer deux nouveaux clients et envoie un packet PacketInHandshake pour
			// s'identifier auprès du serveur.

			// Mets à jour les deux clients tous les 50 ms.

			// La fenetre de menu de jeu sera ouverte pour les 2 clients
			// via le constructeur de PongClient.

			// En cliquant sur les boutons, la fenêtre de jeu s'ouvrira lorsque le serveur
			// indique aux clients qu'ils sont en jeu (via le packet PacketOutTimeUpdate et les fonctions setIdle et setPlaying(opponent_id) de IClient).

			final PongClient client1 = new PongClient(1);
			final PongClient client2 = new PongClient(2);
			
			client1.sendPacket(new PacketInHandshake(client1.user_id));
			client2.sendPacket(new PacketInHandshake(client2.user_id));
			
			while (true) {

				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				client1.update();
				client2.update();
			}
		}
	}

}