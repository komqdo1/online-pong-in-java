package fr.komqdo.client;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

import fr.komqdo.area.Area;
import fr.komqdo.area.Area.Entity;
import fr.komqdo.utils.Scaled;
import fr.komqdo.utils.Size;

/**
 * La fenêtre du jeu.
 * 
 * @author Komqdo
 */

public class GameWindow extends JFrame {

	private Panel panel; 	// Le composant sur lequel on dessine le jeu.

	private Area area;		// L'objet qui représente l'arène avec les raquettes, la balle...
							// et qui est chargé de la conversion en %, et de l'effet mirroir (sur l'axe X).

	public GameWindow(Area area, int user_id) {

		this.area = area;

		// Paramètre la fenêtre (Rien d'intéressant ici)
		
		setTitle("Pong (User " + area.getPlayer().getUserId() + ", agains user " + area.getOpponent().getUserId() + ")");
		
		final Size racket_sz = getArea().racketSize();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocation(50, 50);
		setAlwaysOnTop(true);
		addMouseMotionListener(new MouseListener());
		setSize(getArea().getSize().getWidth() + racket_sz.getWidth() + 1,
				getArea().getSize().getHeight() + racket_sz.getHeight() + 30);

		setResizable(false);

		this.panel = new Panel();

		setContentPane(panel);
		setVisible(true);
	}

	/**
	 * Mets à jour l'affichage.
	 * @author Komqdo
	 */

	public void repaint() {
		this.panel.repaint();
	}

	/**
	 * Ferme la fenetre.
	 * @author Komqdo
	 */

	public void close() {
		dispose();
	}

	public Area getArea() { return (area); }

	/**
	 * Le composant chargé de l'affichage du jeu sur le client.
	 * @author Komqdo
	 */

	public class Panel extends JPanel {

		@Override
		public void paint(Graphics g) {

			final Size racket_sz = getArea().racketSize();
			final Size ball_sz = getArea().ballSize();
			
			// Background

			g.setColor(Color.decode("#66f67"));
			g.fillRect(0, 0, getWidth(), getHeight());

			// Background (contours)

			g.setColor(Color.WHITE);
			g.drawRect(0, 0, getArea().getSize().getWidth() + racket_sz.getWidth(),
					getArea().getSize().getHeight() + racket_sz.getHeight());

			final Entity player = getArea().getPlayer();
			final Entity opponent = getArea().getOpponent();
			final Entity ball = getArea().getBall();

			//  Afficher la raquette du client.

			g.fillRect(player.getLocation().getX(), player.getLocation().getY(),
					racket_sz.getWidth(), racket_sz.getHeight());

			// Afficher la raquette de l'adversaire.

			g.fillRect(opponent.getLocation().getX(), opponent.getLocation().getY(),
					racket_sz.getWidth(), racket_sz.getHeight());

			// Afficher la balle.
			
			g.fillOval(ball.getLocation().getX(), ball.getLocation().getY(),
					ball_sz.getWidth(), ball_sz.getHeight());
		}
	}

	/**
	 * Composant chargé de détecter le mouvement de la souris,
	 * pour mettre à jour la position de la raquette du client.
	 * 
	 * Le client envoie la position de sa raquette tous les 50 ms.
	 * 
	 * @author Komqdo
	 */

	public class MouseListener implements MouseMotionListener {

		private void moveRacket(MouseEvent e) {

			final Scaled scaled = new Scaled(0, e.getComponent().getHeight());
			
			final int percent = scaled.percentFor(e.getY() - 15);

			// Converti le Y de la souris (relatif à la window) et la convertie en %,
			// Puis mets à jour la position de la raquette du client.

			// Le client envoie la position de sa raquette tous les 50 ms.

			getArea().getPlayer().getLocation().setY(percent);
		}

		@Override
		public void mouseDragged(MouseEvent e) {
			moveRacket(e);
		}

		@Override
		public void mouseMoved(MouseEvent e) {
			moveRacket(e);
		}
	}
}
