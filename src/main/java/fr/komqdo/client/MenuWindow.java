package fr.komqdo.client;

import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * La fenêtre du menu avec le bouton "Match making" et "Dual friend".
 * Les boutons envoient au serveur la requête de duel.
 * 
 * @author Komqdo
 */

public class MenuWindow extends JFrame {

	MenuWindow(IClient client, int user_id) {

		super("Pong (User " + user_id + ")");

		// Paramètre la fenêtre (Rien d'intéressant ici)

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocation(50, 50);
		setAlwaysOnTop(true);

		setResizable(false);

		final JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		
		final JButton match_making_button = new JButton("Match making");
		final JButton dual_friend_button = new JButton("Dual friend");

		// Donne une action pour les boutons.

		match_making_button.addActionListener((a)->{
			System.out.println("[LOG] send match making packet.");
			client.sendDualPacket(); // Requête de duel en match making.
		});

		dual_friend_button.addActionListener((a)->{
			System.out.println("[LOG] send dual packet.");
			client.sendDualPacket(user_id == 1 ? 2 : 1); // Requête de duel selon l'ID de l'utilisateur.
		});

		panel.add(match_making_button);
		panel.add(dual_friend_button);

		setContentPane(panel);
		setVisible(true);
		pack();
	}

}
