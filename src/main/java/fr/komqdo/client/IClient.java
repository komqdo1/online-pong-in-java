package fr.komqdo.client;

/**
 * Contient les comportements du client sans le code java.
 * 
 * @author Komqdo
 */

public interface IClient {

	/**
	 * Fonction appelée qaund le serveur indique au client qu'il n'est
	 * pas en jeu.
	 * 
	 * @author Komqdo
	 */

	public void setIdle();

	/**
	 * Fonction appelée quand le serveur indique au client qu'il est en
	 * jeu contre l'utilisateur spécifié.
	 * 
	 * @param opponent_id L'ID de son adversaire.
	 * 
	 * @author Komqdo
	 */

	public void setPlaying(int opponent_id);

	/**
	 * Mets à jour l'affichage du client.
	 * @author Komqdo
	 */

	public void repaint();
	
	/**
	 * Le client envoi un packet KeepAlive qui indique au serveur qu'il est
	 * actuellement connecté au serveur. Ce packet donne aussi la hauteur (%) de la raquette
	 * du joueur client.
	 * 
	 * Le serveur peut ainsi informer l'adversaire du client de la position de la raquette du client.
	 * Le serveur peut également s'assurer que le client est encore connecté.
	 * 
	 * @author Komqdo
	 */

	public void sendKeepAlivePacket();

	/**
	 * Le client envoi une requette pour affronter un joueur au hasard.
	 * @author Komqdo
	 */
	
	public void sendDualPacket();
	
	/**
	 * Le client envoi une requette pour affronter le joueur spécifié.
	 * 
	 * @param opponent_id L'ID du joueur à défier.
	 * 
	 * @author Komqdo
	 */

	public void sendDualPacket(int opponent_id);
	
	/**
	 * Le client envoie une requette pour annuler sa demande de duel (Que ce soit en match making, ou
	 * en duel contre un autre joueur).
	 * 
	 * Si le match a déjà démarré, la cancellation ne sera pas prise en compte.
	 * 
	 * @author Komqdo
	 */

	public void sendDualCancelPacket();
	
	/**
	 * Cette fonction est appelée tous les 50 ms pour mettre à jour
	 * l'affichage du client et envoyer la requête KeepAlive au serveur.
	 * 
	 * Mets à jour le client (Affichage et envoi une requête KeepAlive
	 * pour informer le serveur que le client est bien connecté, et l'information
	 * sur sa raquette s'il est en jeu).
	 * 
	 * @author Komqdo
	 */

	public default void update() {
		repaint();
		sendKeepAlivePacket();
	}
}
