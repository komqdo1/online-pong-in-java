package fr.komqdo.utils;

/**
 * Permets de stocker une taille convertissable en pixel
 * et en % de pixel.
 * 
 * @author Komqdo
 */

public class Size {

	public Scaled scaled_width;
	public Scaled scaled_height;
	public Scaled scale;
	
	public Size(int width, int height) {
		scaled_width = new Scaled(0, width);
		scaled_height = new Scaled(0, height);
	}

	/**
	 * @param with_pcent La largeur en %.
	 * @param height_pcent La longueur en %.
	 * 
	 * @return Une taille de la largeur/longueur définie en paramètre (%) de cet objet.
	 * 
	 * @author Komqdo
	 */

	public Size sizeFor(int with_pcent, int height_pcent)
	{
		final int width = scaled_width.valueFor(with_pcent);
		final int height = scaled_height.valueFor(height_pcent);
		
		return (new Size(width, height));
	}
	
	public int getWidth() { return scaled_width.getMax(); }
	public int getHeight() { return scaled_height.getMax(); }

	public Scaled getScaledWidth() { return (scaled_width); }
	public Scaled getScaledHeight() { return (scaled_height); }
}
