package fr.komqdo.utils;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

import fr.komqdo.packet.Packet;
import fr.komqdo.server.PongServer.Match;

/**
 * La classe qui gère la communication socket du serveur avec le client.
 * @author Komqdo
 */

public abstract class SocketServer {

	protected List<Connected> connecteds;

	private Thread accept_socket_thread;
	private ServerSocket ssocket;

	public SocketServer() {

		this.connecteds = new ArrayList<>();
		
		try {

			this.ssocket = new ServerSocket(1234);

			this.accept_socket_thread = new Thread(()->
			{
				while(true) {

					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
						System.exit(0);
					}
					
					try {
						
						final Socket socket = ssocket.accept();

						System.out.println("[LOG] " + socket + ": connected.");
						connecteds.add(new Connected(socket));

					} catch (IOException e) {
						e.printStackTrace();
						System.exit(0);
					}
				}
			});
			accept_socket_thread.start();

		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	public void sendPacket(Connected connected, Packet packet) {
		try {
			connected.writer.writeObject(packet);
			connected.writer.flush();
		} catch (SocketException | EOFException e) {
			System.err.println("[LOG] " + connected.socket + ": write failed: lost.");
			return ;
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("[ERR] write failed.");
			System.exit(1);
		}
	}
	
	public abstract void receivePacket(Connected connected, Packet packet);
	
	/**
	 * Représente un utilisateur connecté au serveur.
	 * 
	 * @author Komqdo
	 */
	
	public class Connected {
		
		public long last_socket_timestamp;
		public Socket socket;
		public int user_id;
		public int ping;
		public Integer opponent_id;
		public Match match;

		private Thread read_socket;
		private boolean closed;
		private ObjectInputStream reader;
		private ObjectOutputStream writer;

		public Connected(Socket socket) {

			this.socket = socket;
			this.last_socket_timestamp = System.currentTimeMillis();
			
			try {
				this.writer = new ObjectOutputStream(this.socket.getOutputStream());
				this.reader = new ObjectInputStream(this.socket.getInputStream());
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
			
			this.read_socket = new Thread(()->
			{
				while (!closed) {

					try {
						final Object obj = this.reader.readObject();
						
						if (obj instanceof Packet) {
							SocketServer.this.receivePacket(this, (Packet) obj);
						}

					} catch (SocketException | EOFException e) {
						System.out.println("[LOG] " + this.socket + ": end of stream.");
						close();
					} catch (ClassNotFoundException | IOException e) {
						e.printStackTrace();
						System.exit(1);
					}
					
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						e.printStackTrace();
						System.exit(1);
					}
				}
			});

			this.read_socket.start();		
		}

		public void close() {
			closed = true;
			SocketServer.this.connecteds.remove(this);

			if (this.match != null) {
				this.match.close();
			}
		}

		public int getUserId() {
			return (user_id);
		}

		public int getPing() {
			return (ping);
		}
		
		public boolean isTimeout() {
			return (System.currentTimeMillis() - this.last_socket_timestamp >= 5000);
		}

		public boolean hasMatch() {
			return (match != null);
		}
	}
}
