package fr.komqdo.utils;

/**
 * Représente une position 2D dans l'espace.
 * 
 * Peut être retournée via "to_left".
 * 
 * Cet objet permet de convertir des coordonnées en pixel en %,
 * relativement à sa scale.
 * 
 * @author Komqdo
 */

public class Location {

	Scaled scaled_x;		// La position X convertissable en %
	Scaled scaled_y;		// La position Y convertissable en %
	private Size scale;		// La scale par rapport à laquelle cette position est relative (%).
	boolean to_left;		// Si la position est retournée dans l'axe X (Effet mirroir).

	public Location(Size scale) {
		this.scale = scale;
		scaled_x = new Scaled(scale.getScaledWidth());
		scaled_y = new Scaled(scale.getScaledHeight());
	}

	/**
	 * Défini si la position X doit être inversée (Donc aller vers la gauche).
	 * 
	 * @param to_left True si la position X doit être inversée.
	 * 
	 * @author Komqdo
	 */

	public void setToLeft(boolean to_left) {
		this.to_left = to_left;
	}

	public boolean isToLeft() { return (to_left); }

	/**
	 * Défini la coordonnée X en %.
	 * 
	 * @param x_pcent La nouvelle valeur de l'axe X, en %.
	 * 
	 * @author Komqdo
	 */

	public void setX(int x_pcent) {
		this.scaled_x.setPercent(x_pcent);
	}

	/**
	 * Défini la coordonnée Y en %.
	 * 
	 * @param y_pcent La nouvelle valeur de l'axe Y, en %.
	 * 
	 * @author Komqdo
	 */

	public void setY(int y_pcent) {
		this.scaled_y.setPercent(y_pcent);
	}

	/**
	 * Défini les coordonnées XY en %.
	 * 
	 * @param x_pcent La nouvelle valeur de l'axe X, en %.
	 * @param y_pcent La nouvelle valeur de l'axe Y, en %.
	 * 
	 * @author Komqdo
	 */

	public void setXY(int x_pcent, int y_pcent) {
		setX(x_pcent);
		setY(y_pcent);
	}

	/**
	 * Incrémente la coordonnée X en %.
	 * 
	 * @param x_pcent La valeur de l'incrémentation de l'axe X (en %)
	 * 
	 * @author Komqdo
	 */

	public void addX(int x_pcent) {
		this.scaled_x.add(x_pcent);
	}

	/**
	 * Incrémente la coordonnée Y en %.
	 * 
	 * @param y_pcent La valeur de l'incrémentation de l'axe Y (en %)
	 * 
	 * @author Komqdo
	 */

	public void addY(int y_pcent) {
		this.scaled_y.add(y_pcent);
	}

	/**
	 * @return La valeur de X en pixel, inversée si néccessaire.
	 * @author Komqdo
	 */

	public int getX() {

		if (to_left)
			return (scaled_x.getMax() - scaled_x.getValue());

		return (scaled_x.getValue());
	}

	/**
	 * @return La valeur de Y en pixel.
	 * @author Komqdo
	 */

	public int getY() {
		return (scaled_y.getValue());
	}

	/**
	 * @return La valeur de X en %.
	 * @author Komqdo
	 */

	public int getXPercent() {
		return (scaled_x.getPercent());
	}

	/**
	 * @return La valeur de Y en %.
	 * @author Komqdo
	 */

	public int getYPercent() {
		return (scaled_y.getPercent());
	}
	
	/**
	 * @return La taille sur laquelle cette position se base pour
	 * la conversion en %.
	 * 
	 * @author Komqdo
	 */

	public Size getScale() {
		return (scale);
	}

	/**
	 * @param obj_location La position de l'objet.
	 * @param obj_range La taille de l'objet
	 * @param range La taille du périmètre de détection.
	 * 
	 * @return True si la position de l'objet spécifiée en paramètre se trouve dans le périmètre Y
	 * de la position dans un rayon de range à partir de cette position.
	 * 
	 * Par exemple, obj_location est la position de la balle, obj_range est la taille de balle,
	 * range la taille de la raquette, et cette position serait la position de la raquette.
	 * 
	 * @author Komqdo
	 */

	public boolean isYRange(Location obj_location, Size obj_range, Size range) {
		
		final int this_y1 = getY();
		final int this_y2 = getY() + obj_range.getHeight();
		
		final int y1 = obj_location.getY();
		final int y2 = obj_location.getY() + range.getHeight();
		
		if (this_y1 >= y1 && this_y1 <= y2)
			return (true);

		if (this_y2 >= y1 && this_y1 <= y2)
			return (true);

		if (y1 >= this_y1 && y1 <= this_y2)
			return (true);

		if (y2 >= this_y1 && y2 <= this_y2)
			return (true);

		return (false);
	}	
}
