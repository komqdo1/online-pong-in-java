package fr.komqdo.utils;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import fr.komqdo.packet.Packet;

/**
 * La classe qui gère la communication socket du client avec le serveur.
 * @author Komqdo
 */

public abstract class SocketClient {

	private Socket socket;
	private ObjectOutputStream writer;
	private ObjectInputStream reader;
	private boolean closed;
	private Thread read_thread;

	public SocketClient() {

		try {

			socket = new Socket("localhost", 1234);
			writer = new ObjectOutputStream(this.socket.getOutputStream());
			reader = new ObjectInputStream(this.socket.getInputStream());

			this.read_thread = new Thread(()->{

				while (!closed) {
					
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					Object obj;

					try {
						
						obj = this.reader.readObject();
						
						if (obj instanceof Packet) {
							receivePacket((Packet) obj);
						} else {
							System.err.println("[ERR] not a packet.");
							System.exit(1);
						}

					} catch (ClassNotFoundException | IOException e) {
						e.printStackTrace();
						System.err.println("[ERR] read error.");
						System.exit(1);	
					}
				}
			});
			this.read_thread.start();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	public abstract void receivePacket(Packet packet);

	public void sendPacket(Packet packet) {
		try {
			writer.writeObject(packet);
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("[ERR] write failed.");
			System.exit(1);
		}
	}
	
}
