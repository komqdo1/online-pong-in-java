package fr.komqdo.utils;

/**
 * Permet de stocker une valeur en tick (50 ms).
 * @author Komqdo
 */

public class TickValue {
	
	private long value;

	public TickValue(int value) {
		setValueInTick(value);
	}

	public TickValue(long value) {
		setValue(value);
	}

	public void incrementValue() {
		setValue(getValue() + 1);
	}

	public void decrementValue() {
		setValue(getValue() - 1);
	}

	public void setValue(long value) {
		this.value = value;
	}

	public void setValueInTick(int ticks) {
		this.value = ticks * 20;
	}

	public boolean isTickValue() {
		return (getValue() % 20 == 0);
	}

	public int getTickValue() { return (int) (value / 20); }

	public long getValue() { return (value); }

}
