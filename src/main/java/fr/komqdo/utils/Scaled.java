package fr.komqdo.utils;

/**
 * Permet la conversion d'une valeur en % de pixel vers une valeur
 * en pixels, et vice-versa.
 * 
 * @author Komqdo
 */

public class Scaled {

    private int percent;
    private final int min;
    private final int max;

    public Scaled(int min, int max) {
        this.min = min;
        this.max = max;
        this.percent = 0;
    }

	public Scaled(Scaled other) {
		this.min = other.min;
		this.max = other.max;
	}

    /**
     * @return La valeur minimum en pixel.
     * @author Komqdo
     */

    public int getMin() { return (min); }

    /**
     * @return La valeur maximum en pixel.
     * @author Komqdo
     */

	public int getMax() { return (max); }

    /**
     * @return La valeur en pixel selon la valeur définie du pourcentage.
     * @author Komqdo
     */

    public int getValue() {
        return valueFor(this.percent);
    }

    /**
     * Renvoie la valeur en pixel de la valeur specifiée en %.
     * Par exemple, si la taille est entre 0 et 200, 10% renverra 20 en valeur pixel.
     * 
     * @param percent Une valeur en %.
     * 
     * @return La valeur en pixel de la valeur specifiée en %.
     * 
     * @author Komqdo
     */

	public int valueFor(int percent) {
        if (percent < 0 || percent > 100) {
            throw new IllegalArgumentException("Percent value must be between 0 and 100");
        }

        return (int) (min + ((max - min) * (percent / 100.0)));
    }

    /**
     * Renvoie une valeur en % de la valeur spécifiée en pixel.
     * Par exemple, si la taille est entre 0 et 200, 20 renverra 10%.
     * 
     * @param value Une valeur en pixel.
     * 
     * @return Une valeur en % de la valeur spécifiée en pixel.
     */

    public int percentFor(int value) {
        if (value < min || value > max) {
            throw new IllegalArgumentException("Value must be between min and max");
        }
        return (int) ((value - min) / (double)(max - min) * 100);
    }

    /**
     * Incrémente la valeur en %.
     * 
     * @param percent_to_add Une valeur en % de pixel.
     * 
     * @author Komqdo
     */

    public void add(int percent_to_add) {
        this.percent += percent_to_add;
        if (this.percent < 0) {
            this.percent = 0;
        }
        if (this.percent > 100) {
            this.percent = 100;
        }
    }

    /**
     * Décremente la valeur en %.
     * 
     * @param percent_to_add Une valeur en % de pixel.
     * 
     * @author Komqdo
     */

    public void remove(int percentToRemove) {
        this.percent -= percentToRemove;
        if (this.percent < 0) {
            this.percent = 0;
        }
        if (this.percent > 100) {
            this.percent = 100;
        }
    }

    /**
     * @return Le pourcentage actuel.
     */

    public int getPercent() {
        return (percent);
    }

    /**
     * Défini le pourcentage actuel.
     * 
     * @param percent Une valeur en % de pixel.
     * 
     * @return Cet objet.
     * 
     * @author Komqdo
     */

    public Scaled setPercent(int percent) {
        if (percent >= 0 && percent <= 100) {
            this.percent = percent;
        } else {
            throw new IllegalArgumentException("Percent value must be between 0 and 100");
        }
		return (this);
    }
}
