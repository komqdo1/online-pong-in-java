package fr.komqdo.packet.in;

import fr.komqdo.packet.Packet;

/**
 * Ce packet est envoyé par le client au serveur pour indiquer au serveur
 * qu'il veut duel un autre joueur (Match making ou Dual friend).
 * 
 * Pour duel un joueur au hasard (Match making), opponent_id doit
 * être défini à 0.
 * 
 * Ce packet est envoyé via les boutons "Match making" et "Dual friend"
 * dans MenuWindow.
 * 
 * @author Komqdo
 */

public class PacketInDual extends Packet {
	
	public int opponent_id;

	public PacketInDual(int opponent_id) {
		this.opponent_id = opponent_id;
	}
}
