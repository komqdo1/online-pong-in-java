package fr.komqdo.packet.in;

import fr.komqdo.packet.Packet;

/**
 * Packet envoyé par le client au serveur pour s'identifier auprès du serveur.
 * 
 * Le serveur peut indentifier une connexion par un user_id.
 * 
 * Ce packet est envoyé juste après avoir établi la connexion avec
 * le serveur.
 * 
 * @author Komqdo
 */

public class PacketInHandshake extends Packet {
	
	public int user_id;

	public PacketInHandshake(int user_id) {
		this.user_id = user_id;
	}

}
