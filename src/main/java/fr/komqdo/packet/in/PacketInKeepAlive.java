package fr.komqdo.packet.in;

import fr.komqdo.packet.Packet;

/**
 * Packet envoyé par le client au serveur pour indiquer au serveur que le client
 * est toujours connecté.
 * 
 * Si le client est en jeu, le packet contient également la hauteur de la
 * raquête du client.
 * 
 * Ce packet est envoyé par le client au serveur tous les 50 ms pour maintenir
 * la connexion et informer le serveur la position de la raquette du client.
 * 
 * @author Komqdo
 */

public class PacketInKeepAlive extends Packet {
	
	public Integer y_pcent;
	public long timestamp;

	public PacketInKeepAlive(Integer y_pcent) {
		this.y_pcent = y_pcent;
		this.timestamp = System.currentTimeMillis();
	}
}
