package fr.komqdo.packet.in;

import fr.komqdo.packet.Packet;

/**
 * Ce packet est envoyé par le client au serveur pour indiquer au serveur
 * qu'il veut annuler sa demande de duel (Match making ou Dual friend).
 * 
 * Ce packet peut être appelé via un bouton "Cancel" pour annuler la demande
 * de duel.
 * 
 * @author Komqdo
 */

public class PacketInDualCancel extends Packet {
	
}
