package fr.komqdo.packet;

import java.io.Serializable;

/**
 * Classe abstraite d'un packet envoyé entre les client et le serveur.
 * 
 * @author Komqdo
 */

public abstract class Packet implements Serializable {
	
}
