package fr.komqdo.packet.out;

import fr.komqdo.packet.Packet;

/**
 * Packet envoyé par le serveur au client tous les 50 ms.
 * 
 * Ce packet permet d'informer le joueur de s'il est en partie ou non. C'est ce
 * packet qui mets à jour les fonctions setIdle et setPlaying(opponent_id) du IClient.
 * 
 * Si le joueur est en partie, les valeurs ball_x_pcent, ball_y_pcent, to_left, opponent_y_pcent,
 * time, opponent_id sont valeurisés.
 * 
 * @author Komqdo
 */

public class PacketOutTimeUpdate extends Packet {
	
	public Integer ball_x_pcent;
	public Integer ball_y_pcent;
	public Boolean to_left;

	public Integer opponent_y_pcent;

	public Integer time;
	public Integer opponent_id;
	public boolean playing;

	public PacketOutTimeUpdate(Integer ball_x_pcent, Integer ball_y_pcent, Boolean to_left,
			Integer opponent_y_pcent, Integer opponent_id, boolean playing, Integer time) {

		this.ball_x_pcent = ball_x_pcent;
		this.ball_y_pcent = ball_y_pcent;
		this.to_left = to_left;
		this.opponent_y_pcent = opponent_y_pcent;
	
		this.time = time;
		this.opponent_id = opponent_id;
		this.playing = playing;
	}

	public boolean isPlaying() {
		return (playing);
	}

	public int getOpponentId() {
		
		if (!isPlaying()) {
			throw new RuntimeException("Not currently playing.");
		}

		return (opponent_id);
	}
}
