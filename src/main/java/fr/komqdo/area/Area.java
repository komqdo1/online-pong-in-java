package fr.komqdo.area;

import fr.komqdo.utils.Location;
import fr.komqdo.utils.Size;

/**
 * Cette classe est utilisée par le serveur et par le client pour contenir les composants
 * d'une partie (balle et raquettes).
 * 
 * @author Komqdo
 */

public class Area {

	private Size size = new Size(375, 250);

	private Racket opponent;
	private Racket player;
	private Entity ball;

	public Area(int player_id, int opponent_id) {
		this(new Size(375, 250), player_id, opponent_id);
	}

	public Area(Size size, int player_id, int opponent_id) {

		this.size = size;

		this.player = new Racket(player_id);
		this.player.getLocation().setY(50);

		this.opponent = new Racket(opponent_id);
		this.opponent.getLocation().setY(50);
		this.opponent.getLocation().setToLeft(true);

		this.ball = new Entity();
		this.ball.getLocation().setXY(50, 50);
	}

	public Size racketSize() {
		return (getSize().sizeFor(2, 10));
	}
	
	public Size ballSize() {
		return (getSize().sizeFor(3, 5));
	}

	public Racket getOpponent() { return (opponent); }
	public Racket getPlayer() { return (player); }
	public Entity getBall() { return (ball); }
	public Size getSize() { return (this.size); }

	/**
	 * Représente une entitée (Une raquette ou la balle)
	 * @author Komqdo
	 */

	public class Entity {
		
		Location location = new Location(getSize());

		public Location getLocation() { return (location); }
	}

	/**
	 * Représente une raquette (Hérite de Entity).
	 * 
	 * @author Komqdo
	 */

	public class Racket extends Entity {

		private final int user_id;

		public Racket(int user_id) {
			this.user_id = user_id;
		}

		public int getUserId() { return (user_id); }
	}
}
